//http://api.openweathermap.org/data/2.5/forecast?q=mumbai&units=metric&APPID=bd3f5ad55d94b4780cb343db35b62cb8

const key = "bd3f5ad55d94b4780cb343db35b62cb8";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
//    console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error("Error Status: "+response.status);
    }
}

//getForecast()
//    .then(data => console.log(data))
//    .catch(err => console.error(err));